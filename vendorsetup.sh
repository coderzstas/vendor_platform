for combo in $(curl -s https://raw.githubusercontent.com/ay-securityos/hudson/master/aysecurity-build-targets | sed -e 's/#.*$//' | grep aysecurity-10 | awk '{printf "aysecurity_%s-%s\n", $1, $2}')
do
    COMMON_LUNCH_CHOICES += " ${combo}"
done
