# Build fingerprint
ifneq ($(BUILD_FINGERPRINT),)
ADDITIONAL_BUILD_PROPERTIES += \
    ro.build.fingerprint=$(BUILD_FINGERPRINT)
endif

# aysecurity System Version

ADDITIONAL_BUILD_PROPERTIES += \
    ro.aysecurity.version=$(PRODUCT_VERSION_MAJOR).$(PRODUCT_VERSION_MINOR) \
    ro.aysecurity.releasetype=NIGHTLY \
    ro.aysecurity.build.version=$(PRODUCT_VERSION_MAJOR).$(PRODUCT_VERSION_MINOR) \
    ro.modversion=$(AYSECURITY_VERSION) \
    ro.aysecurity.url=https://os.aysecurity.net/legal \
    aysecurityos.updater.uri=http://ota.aysecurity.net/api/v1/{device}/{type} \
    aysecurityos.device.info=AY.Phone

# aysecurity Platform Display Version
ADDITIONAL_BUILD_PROPERTIES += \
    ro.aysecurity.display.version=$(AYSECURITY_DISPLAY_VERSION)


ADDITIONAL_BUILD_PROPERTIES += \
    ro.lineage.build.version.plat.sdk=$(LINEAGE_PLATFORM_SDK_VERSION) \
    ro.lineage.build.version.plat.rev=$(LINEAGE_PLATFORM_REV)
