# Inherit common mobile aysecurity stuff
$(call inherit-product, vendor/aysecurity/config/common.mk)

# Default notification/alarm sounds
PRODUCT_PRODUCT_PROPERTIES += \
    ro.config.notification_sound=End_note.ogg \
    ro.config.alarm_alert=Bright_morning.ogg

ifneq ($(TARGET_BUILD_VARIANT),user)
# Thank you, please drive thru!
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += persist.sys.dun.override=0
endif

# Optional packages
PRODUCT_PACKAGES += \
    LiveWallpapersPicker \
    PhotoTable

# AOSP packages
PRODUCT_PACKAGES += \
    ExactCalculator \
    Exchange2

# aysecurity packages
PRODUCT_PACKAGES += \
    AudioFX \
    Backgrounds \
    Etar \
    LockClock \
    Profiles \
    TrebuchetQuickStep \
    WeatherProvider

ifeq ($(WITH_AYSECURITY_UPDATER),true)
    PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
        ro.aysecurity.extra.prop = TODO
endif

# Accents
PRODUCT_PACKAGES += \
    LineageBlackTheme \
    LineageDarkTheme \
    LineageBlackAccent \
    LineageBlueAccent \
    LineageBrownAccent \
    LineageCyanAccent \
    LineageGreenAccent \
    LineageOrangeAccent \
    LineagePinkAccent \
    LineagePurpleAccent \
    LineageRedAccent \
    LineageYellowAccent

# Extra accents
ifeq ($(EXTRA_ACCENTS),true)

# Accents from crDroid
PRODUCT_PACKAGES += \
    Amber \
    BlueGrey \
    DeepOrange \
    DeepPurple \
    Grey \
    Indigo \
    LightBlue \
    LightGreen \
    Lime \
    Teal \
    White

# Brand Accents from crDroid
PRODUCT_PACKAGES += \
    AospaGreen \
    AndroidOneGreen \
    CocaColaRed \
    DiscordPurple \
    FacebookBlue \
    InstagramCerise \
    JollibeeCrimson \
    MonsterEnergyGreen \
    NextbitMint \
    OneplusRed \
    PepsiBlue \
    PocophoneYellow \
    RazerGreen \
    SamsungBlue \
    SpotifyGreen \
    StarbucksGreen \
    TwitchPurple \
    TwitterBlue \
    XboxGreen \
    XiaomiOrange
endif

# Charger mode images
PRODUCT_PACKAGES += \
    charger_res_images \
    product_charger_res_images

# Customizations
PRODUCT_PACKAGES += \
    LineageNavigationBarNoHint

# Media
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    media.recorder.show_manufacturer_and_model=true

