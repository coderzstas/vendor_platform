# Inherit mini common aysecurity stuff
$(call inherit-product, vendor/aysecurity/config/common_mini.mk)

# Required packages
PRODUCT_PACKAGES += \
    LatinIME

$(call inherit-product, vendor/aysecurity/config/telephony.mk)
