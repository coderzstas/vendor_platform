# Inherit full common aysecurity stuff
$(call inherit-product, vendor/aysecurity/config/common_full.mk)

# Required packages
PRODUCT_PACKAGES += \
    LatinIME

# Include aysecurity LatinIME dictionaries
PRODUCT_PACKAGE_OVERLAYS += vendor/aysecurity/overlay/dictionaries
